COMPILEFLAGS=-g
CCOMP=gcc

phrase : main.o phrase.o
	$(CCOMP) $(COMPILEFLAGS) -o phrase main.o phrase.o

main.o : main.c phrase.h
	$(CCOMP) -c $(COMPILEFLAGS)  main.c
phrase.o : phrase.c phrase.h
	$(CCOMP) -c $(COMPILEFLAGS)  phrase.c
clean:
	rm main.o phrase.o 
