// Samantha Deshazer
// 11/14/19
// CSE224
// PA4:
// This program takes file with text and partitions them into
// phrases seperated by specific character. It outputs them in
// uppercase format with the number of times the phrase occurs.

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
//-----------------
#include "phrase.h"
//-----------------

int main(void) {
  //declaration of structure:
  Phrase phrase;
  //read in file one character at a time:
  readfile(&phrase);
  printf("without attempted sort:\n");
  printphrases(&phrase);
  printf("\n\n");
  //sort phrase occurances in decending order:
  printf("with attempted sort:\n");
  bubblesort(&phrase);
  //print the phrases:
  printphrases(&phrase);
  printf("\n");
  return 0;
}//main


