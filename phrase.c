#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "phrase.h"


//------------------------
int isendofphrase(int c) {
  if (c == ',' || c == '.' || c == ';'
     || c == ':' || c == '?' || c == '!'){

    return 1;

  }//if


  return 0;
}//isendofphrase


//-----------------
int ignore(int c){
  if (c == '"' || c == '('
   || c == ')' || c == '-'
   || c == '\''){

    return 1;


  }//if

  return 0;
}//ignore


//------------------------------------------
int checkphrase(Phrase* phrase, int index){
  //phrase -> dictlen gets the member called dictlen from
  // the structure that is the phrase.
  for (int i = 0; i < phrase->dictlen; i++){

    if (strcmp(phrase->dictionary[i], phrase->dictionary[index]) == 0){

      // there is a duplicate. increment count.
      phrase->count[i]++;
      //flag for duplicate found:
      return 1;

    }//if
  }//for
  //flag to continue:
  return 0;
}//checkphrase


//-----------------------------
void readfile(Phrase* phrase) {

 // printf("reading file...\n");
  phrase->dictlen = 0;

  int c;
  c=getchar();

  int index = 0;
  int counter = 0;


    while (c != EOF){

      if (isendofphrase(c) == 1){ //.!? found
        if (checkphrase(phrase, index) == 1){ // duplicate found
          //fills the first bits of memory:
          memset(phrase->dictionary[index], 0, 201);
        }//if
        else{
          index++;
          if((phrase->dictlen)==1001){

	     break;
          }else{
            phrase->dictlen++;
          }//else
        }//else

        counter = 0;
      }//if
      //turn all new lines into a space:
      if (c=='\n'){

          c = ' ';

      }//if

      if (isendofphrase(c) == 0
      && ignore(c) == 0
      && !(c == ' ' && counter == 0)){
        //if no magic chars found
        // and if its not a space and the counter is 0
        if (phrase->count[phrase->dictlen] == 0){

          phrase->count[phrase->dictlen]++;

        }//if


        c = toupper(c);
        //store c:
        //first makesure it isnt full:
        if(counter==200){
          phrase->dictionary[index][201]='\0';
        }else{
          phrase->dictionary[index][counter] = c;
          counter++;
        }//else
      }//if

     c=getchar();

    }//while
}//readfile

//-------------------------------------
int numofphrases(Phrase* phrase){


  int counter = 1;
  for (int i = 0; i < counter; i++){

    if (strlen(phrase->dictionary[i]) > 0){
      //number of phrases:
      counter++;
    }//if
    else{
      return i;
    }//else
  }//for
  return 0;
}//numofphrases


//------------------------------
void bubblesort(Phrase* phrase){

  int* temp;
  // sort phrase occurances in decending order:

  for (int i = 0 ; i < ((numofphrases(phrase)) - 1); i++){

    for (int j = 0; j < ((numofphrases(phrase)) - i - 1); j++) {

      if (phrase->count[j] < phrase->count[j+1]){

        temp=&phrase->count[j];

        phrase->count[j]=phrase->count[j+1];
        phrase->count[j+1]=*temp;

      }//if
    }//for
  }//for
}//bubblesort


//--------------------------------
void printphrases(Phrase* phrase){

  for (int i = 0; i < numofphrases(phrase); i++){

    printf("%05d", phrase->count[i]);
    printf("<%s>", phrase->dictionary[i]);

    if (i < (numofphrases(phrase) - 1)){

      printf("\n");

    }//if
  }//for
}//printphrases



