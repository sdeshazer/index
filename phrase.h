//Phrase.h
#include <stdio.h>

//--------------------------
//struct
typedef struct StorePhrase {
  int count[1000];
  char dictionary[1000][201];
  int dictlen;
} Phrase;


//---------------------------------------------
// Prototypes of phrase.c:
void printphrases(Phrase* phrase);
void readfile(Phrase* phrase);
int numofphrases(Phrase* phrase);
int checkphrase(Phrase* phrase, int index);
int ignored(int c);
int isendofphrase(int c);
void bubblesort(Phrase* phrase);
//void swap(int*, int*);
